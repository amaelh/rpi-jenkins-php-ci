.PHONY: all install run test clean

DOCKER_IMAGE_NAME := amaelh/rpi-jenkins-php-ci
DOCKER_IMAGE_VERSION=2.64
DOCKER_IMAGE_TAGNAME=$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_VERSION)

all: build push install run test

clean:
	docker-compose stop
	docker-compose rm -f

build:
	docker build -t $(DOCKER_IMAGE_TAGNAME) .
	docker tag $(DOCKER_IMAGE_TAGNAME) $(DOCKER_IMAGE_NAME):latest

push:
	docker push $(DOCKER_IMAGE_NAME)


install:
	mkdir -p ./jenkins_home

run:
	docker-compose stop
	docker-compose up -d --remove-orphans

test:
	docker exec $(shell docker ps -a | grep $(DOCKER_IMAGE_NAME) | awk '{print $$1}') /bin/echo "Success."
