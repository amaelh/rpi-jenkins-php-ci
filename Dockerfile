FROM jsurf/rpi-raspbian:latest
MAINTAINER Amael H. <amael@heiligtag.com>

# Jenkins version
ENV JENKINS_VERSION 2.64

# Other env variables
ENV JENKINS_HOME /var/jenkins_home
ENV JENKINS_SLAVE_AGENT_PORT 50000

# Enable cross build
#RUN ["cross-build-start"]

# Install dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends curl openjdk-8-jdk wget \
    && rm -rf /var/lib/apt/lists/*

# Install jenkins
RUN wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add - \
    && sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list' \
    && apt-get update \
    && apt-get install -y --no-install-recommends jenkins \
    && rm -rf /var/lib/apt/lists/*

# Disable cross build
#RUN ["cross-build-end"]

# Expose volume
VOLUME ${JENKINS_HOME}

# Working dir
WORKDIR ${JENKINS_HOME}

ADD setup.sh /setup.sh
ADD example.xml /example.xml
RUN sh /setup.sh

# Expose ports
EXPOSE 8080 ${JENKINS_SLAVE_AGENT_PORT}


CMD service jenkins start && tail -f /var/log/jenkins/jenkins.log
